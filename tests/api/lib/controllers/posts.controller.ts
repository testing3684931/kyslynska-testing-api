import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async createNewPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
   
    async getAllPosts(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addLikeToPost(accessToken: string, likeData: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(likeData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
   
    async addCommentToPost(accessToken: string, commentData: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(commentData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
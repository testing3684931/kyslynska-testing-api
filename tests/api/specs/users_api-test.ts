import { UsersController } from './../lib/controllers/users.controller';
import { expect } from 'chai';
import { checkResponseTime, checkSchema, checkStatusCode } from '../../helpers/functionsForChecking.helper';

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const usersController = new UsersController();
const myUserData = global.appConfig.users.Alina;
const newUserData = global.appConfig.users.Svetlana;


describe(`Users controller`, () => {
    let userId: number;
    let accessToken: string;

    it(`Should register new user and get tokens`, async () => {
        const response = await usersController.registerNewUser(myUserData);

        checkStatusCode(response, 201);
        checkResponseTime(response, 2000);
        checkSchema(response, schemas.schema_registerUser)

        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;

        expect(response.body.user.id, `User id in the response should be more than 0`).to.be.greaterThan(0); 
        expect(response.body.user.email, `User email in the response should be equal initial data`).to.be.equal(myUserData.email); 
        expect(response.body.user.userName, `User name in the response should be equal initial data`).to.be.equal(myUserData.userName); 
        expect(response.body.token.refreshToken, `Response should have refreshToken`).to.not.be.undefined;
        expect(response.body.token.accessToken.token, `Response should have accessToken`).to.not.be.undefined;
    });

    it(`Get all users`, async () => {
        const response = await usersController.getAllUsers(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        checkSchema(response, schemas.schema_getAllUsers);
    });

    it(`Login user`, async () => {
        const response = await usersController.loginUser(myUserData.email, myUserData.password);

        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
        checkSchema(response, schemas.schema_loginUser);

        expect(response.body.user.userName, `User name in the response should be equal initial data`).to.be.equal(myUserData.userName); 
    });

    it(`Get user data by token`, async () => {
        const response = await usersController.getDataUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
        checkSchema(response, schemas.schema_getDateUser);

        expect(response.body.email, `User email in the response should be equal initial data`).to.be.equal(myUserData.email); 
        expect(response.body.userName, `User name in the response should be equal initial data`).to.be.equal(myUserData.userName);
    });

    it(`Update user`, async () => {
        newUserData.id = userId;
        const response = await usersController.updateUser(newUserData, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 2000);

        expect(response.headers.server, `Response server header should be Kestrel`).to.be.equal('Kestrel'); 
    });

    it(`Get user data by token for check updated user data`, async () => {
        const response = await usersController.getDataUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
        checkSchema(response, schemas.schema_getDateUser);
        
        expect(response.body.avatar, `User avatar in the response should be updated`).to.be.equal(newUserData.avatar);
        expect(response.body.email, `User email in the response should be updated`).to.be.equal(newUserData.email); 
        expect(response.body.userName, `User userName in the response should be updated`).to.be.equal(newUserData.userName);
    });

    it(`Get user by Id`, async () => {
        const response = await usersController.getUserById(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
        checkSchema(response, schemas.schema_getUserById);
  
        expect(response.body.avatar, `User avatar in the response should be updated`).to.be.equal(newUserData.avatar);
        expect(response.body.email, `User email in the response should be updated`).to.be.equal(newUserData.email); 
        expect(response.body.userName, `User userName in the response should be updated`).to.be.equal(newUserData.userName);
    });

    it(`Check with invalid data set for login`, async () => {
        const invalidUserDataSet = [
            { email: myUserData.email, password: myUserData.password },
            { email: 'la2201r@gmail.com', password: '      ' },
            { email: 'la2201r@gmail.com', password: 'password' },
            { email: 'la2201r@gmail.com', password: '123' },
            { email: 'la2201r@gmail.com', password: '' },
            { email: 'la2201r@gmail.com', password: 'la2201r@gmail.com' },
        ];

        invalidUserDataSet.forEach( async (credentials) => {
            const response = await usersController.loginUser (credentials.email, credentials.password);

            checkStatusCode(response, 401); 
            checkResponseTime(response, 3000);
        });
    });


    it(`Delete user`, async () => {
        const response = await usersController.deleteUser(newUserData.id, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response,2000);

        expect(response.headers.server, `Response server header should be Kestrel`).to.be.equal('Kestrel');
    });
});

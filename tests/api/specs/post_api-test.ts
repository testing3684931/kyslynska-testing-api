import { PostsController } from './../lib/controllers/posts.controller';
import { UsersController } from './../lib/controllers/users.controller';
import { expect } from 'chai';
import { checkResponseTime, checkSchema, checkStatusCode } from '../../helpers/functionsForChecking.helper';

const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const usersController = new UsersController();
const postsController = new PostsController();
const myUserData = global.appConfig.users.Alina;
const myPostData = global.appConfig.data.postData;

describe(`Posts controller`, () => {
    let postId: number;
    let commentId: number;
    let accessToken: string;
    let userId: number

    before(`Register new user and login`, async () => {
        const response = await usersController.registerNewUser(myUserData);

        userId = response.body.user.id;
        accessToken = response.body.token.accessToken.token;

        await usersController.loginUser(myUserData.email, myUserData.password);
    });

    after(`Delete user`, async () => {
        await usersController.deleteUser(userId, accessToken);
    });

    it(`Create new post`, async () => {
        const response = await postsController.createNewPost(myPostData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        checkSchema(response, schemas.schema_createNewPost);

        postId = response.body.id;

        expect(response.body.body, `Post body in the response should be equal initial data`).to.be.equal(myPostData.body); 
        expect(response.body.author.id, `Author id in the response should be equal user id`).to.be.equal(userId); 
    });

    it(`Get all posts for search created post`, async () => {
        const response = await postsController.getAllPosts(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        const myPost = response.body.find((post) => post.id === postId);

        expect(myPost, `Created post should be present in response`).to.not.be.undefined; 
    });  

    it(`Add like to my post`, async () => {
        const likeData = {
            entityId: postId,
            isLike: true,
            userId: userId
        }

        const response = await postsController.addLikeToPost(accessToken, likeData);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });  

    it(`Get all posts for searching created Like of post`, async () => {
        const response = await postsController.getAllPosts(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        const myPost = response.body.find((post) => post.id === postId);
        const myLike = myPost.reactions.find((reaction) => reaction.user.id === userId);

        expect(myLike, `Created like should be present in response`).to.not.be.undefined; 
    });

    it(`Add comment to post`, async () => {
        const commentData = {
            authorId: userId,
            postId: postId,
            body: "This is a great exercise"
        }

        const response = await postsController.addCommentToPost(accessToken, commentData);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        commentId = response.body.id;

        expect(response.body.body, `Comment body in the response should be equal initial data`).to.be.equal(commentData.body); 
        expect(response.body.author.id, `Author id in the response should be equal user id`).to.be.equal(userId); 
    });

    it(`Get all posts for searching created Like of post.`, async () => {
        const response = await postsController.getAllPosts(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        const myPost = response.body.find((post) => post.id === postId);
        const myComment = myPost.comments.find((comment) => comment.id === commentId);

        expect(myComment, `Created comment should be present in reesponse`).to.not.be.undefined;
        expect(myComment.id, `Comment id in the response should be equal created comment id`).to.be.equal(commentId); 
    });

    it(`Create new post with invalid token`, async () => {
        const invalidAccessToken =  '0'
        const response = await postsController.createNewPost(myPostData, invalidAccessToken);

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });
})
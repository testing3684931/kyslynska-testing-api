import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class UsersController {
    async registerNewUser(userData: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(userData)
            .send();
        return response;
    }

    async getAllUsers(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async loginUser(email: string, password: string) {
        const userData = {
            email: email,
            password: password
          }

        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/auth/login`)
            .body(userData)
            .send();
        return response;
    }

    async getDataUser(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async deleteUser(id: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`api/Users/${id}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    
    async updateUser(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getUserById(id: number) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/${id}`)
            .send();
        return response;
    }
}